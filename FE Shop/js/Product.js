
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/"
var accessToken = localStorage.getItem('token');
console.log(accessToken)

var gDataProductLine = [];
var gDataProduct = [];
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onLoadProductPhanTrang(0);
    onLoadProductLine();
    onLoadProduct();

    // phan trang
    $("#page2").on("click", function () {
        $("#row-product-all").html("");
        onLoadProductPhanTrang(1);
        $("#page2").addClass("active");
        $("#page1").removeClass("active");
        $("#page3").removeClass("active");
        $("#page4").removeClass("active");
        $("#page5").removeClass("active");
    })
    $("#page1").on("click", function () {
        $("#row-product-all").html("");
        onLoadProductPhanTrang(0);
        $("#page1").addClass("active");
        $("#page2").removeClass("active");
        $("#page3").removeClass("active");
        $("#page4").removeClass("active");
        $("#page5").removeClass("active");
    })
    $("#page3").on("click", function () {
        $("#row-product-all").html("");
        onLoadProductPhanTrang(2);
        $("#page3").addClass("active");
        $("#page2").removeClass("active");
        $("#page1").removeClass("active");

        $("#page4").removeClass("active");
        $("#page5").removeClass("active");
    })
    $("#page4").on("click", function () {
        $("#row-product-all").html("");
        onLoadProductPhanTrang(3);
        $("#page4").addClass("active");
        $("#page2").removeClass("active");
        $("#page1").removeClass("active");

        $("#page3").removeClass("active");
        $("#page5").removeClass("active");
    })


    $("#wireless").on("click", function () {
        var vArryKq = onBtnFilterByProductLineWireless();
        $("#row-product-all").html("");
        onFillData(vArryKq);


    })
    $("#in-ear").on("click", function () {
        var vArryKq = onBtnFilterByProductLineInEar();
        $("#row-product-all").html("");
        onFillData(vArryKq[0]);
        console.log(vArryKq[0]);

    })
    $("#over-ear").on("click", function () {
        var vArryKq = onBtnFilterByProductLineOverEar();
        $("#row-product-all").html("");
        onFillData(vArryKq[0]);

    })
    $("#sport-head").on("click", function () {
        var vArryKq = onBtnFilterByProductLineSport();
        $("#row-product-all").html("");
        onFillData(vArryKq[0]);

    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "jbl") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProductBrandJbl();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "samsung") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProductBrandSamSung();
            onFillData(vArrayKq);



        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "sony") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProductBrandSony();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "beat") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProductBrandBeat();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "logistech") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProductBrandLogistech();
            onFillData(vArrayKq);


        }
    })
    // filter by color
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "Red") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProductRed();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "Dark") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProductDark();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "White") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProductWhite();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "Pink") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProductPink();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "Blue") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProductBlue();
            onFillData(vArrayKq);


        }
    })
    // filter by rating
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "1sao") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProduct1Sao();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "2sao") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProduct2Sao();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "3sao") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProduct3Sao();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "4sao") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProduct4Sao();
            onFillData(vArrayKq);


        }
    })
    $('input[type="checkbox"]').change(function () {
        var $this = $(this);
        if ($this.val() == "5sao") {
            $("#row-product-all").html("");
            var vArrayKq = onBtnFilterByProduct5Sao();
            onFillData(vArrayKq);


        }
    })

    // filter by price
    $("#btn-filter-price").on("click", function () {
        var vFirstPrice = $("#price-f").val();
        var vLastPrice = $("#price-l").val();

        console.log(vFirstPrice);
        console.log(vLastPrice);

        var vArryKq = OnFilterByPrice(vFirstPrice, vLastPrice);
        $("#row-product-all").html("");
        onFillData(vArryKq);
        // console.log(vArryKq);


    })
    $("#btn-find-product-name").on("click", function () {
        var vString = $("#inp-find-product-name").val();
        console.log(vString);
        var vArrayData = onFindProductName(vString);
        $("#row-product-all").html("");
        onFillData(vArrayData);
        console.log(vArrayData);
    })

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onLoadProductPhanTrang(vPage) {
    $.ajax({
        url: gBASE_URL + "productP?page=" + vPage + "&size=9",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            // gDataProduct = paramRes;
            onFillData(paramRes);
            console.log(paramRes);

        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
function onLoadProduct() {
    $.ajax({
        url: gBASE_URL + "product",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            gDataProduct = paramRes;
            // onFillData(paramRes);
            console.log(paramRes);

        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
function onLoadProductLine() {
    $.ajax({
        url: gBASE_URL + "productLine",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            gDataProductLine = paramRes;
            console.log(paramRes);

        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
function onBtnFilterByProductLineWireless() {
    var vArrayWireless = [];
    for (var bI = 0; bI < gDataProductLine.length; bI++) {
        if (gDataProductLine[bI].gDataProductLine == "Wireless") {
            vArrayWireless.push(gDataProductLine[bI].products);
        }
    }
    return vArrayWireless;

}
function onBtnFilterByProductLineInEar() {
    var vArrayInEar = [];
    for (var bI = 0; bI < gDataProductLine.length; bI++) {
        if (gDataProductLine[bI].productLine == "In ear Headphone") {

            vArrayInEar.push(gDataProductLine[bI].products);
        }
    }

    return vArrayInEar;

}
function onBtnFilterByProductLineOverEar() {
    var vArrayOverEar = [];
    for (var bI = 0; bI < gDataProductLine.length; bI++) {
        if (gDataProductLine[bI].productLine == "Over Headphone") {

            vArrayOverEar.push(gDataProductLine[bI].products);
        }
    }

    return vArrayOverEar;

}
function onBtnFilterByProductLineSport() {
    var vArraySport = [];
    for (var bI = 0; bI < gDataProductLine.length; bI++) {
        if (gDataProductLine[bI].productLine == "Sport Headphone") {

            vArraySport.push(gDataProductLine[bI].products);
        }
    }

    return vArraySport;

}
function onBtnFilterByProductBrandJbl() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].productVendor == "JBL") {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProductBrandSamSung() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].productVendor == "SAMSUNG") {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProductBrandSony() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].productVendor == "SONY") {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProductBrandBeat() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].productVendor == "BEAT") {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProductBrandLogistech() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].productVendor == "LOGISTECH") {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProductRed() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].color == "Red") {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProductDark() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].color == "Dark") {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProductWhite() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].color == "White") {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProductPink() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].color == "Pink") {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProductBlue() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].color == "Blue") {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProduct1Sao() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].rating == 1) {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProduct2Sao() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].rating == 2) {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProduct3Sao() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].rating == 3) {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProduct4Sao() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].rating == 4) {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
function onBtnFilterByProduct5Sao() {
    var vArrayBrand = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].rating == 5) {

            vArrayBrand.push(gDataProduct[bI]);
        }

    }

    return vArrayBrand;

}
// filter bt price
function OnFilterByPrice(paramFirstPrice, paramLastPrice) {
    var vArrayPrice = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].priceSale >= paramFirstPrice && gDataProduct[bI].priceSale <= paramLastPrice) {

            vArrayPrice.push(gDataProduct[bI]);
        }

    }

    return vArrayPrice;
}
function onFindProductName(paramString) {
    var vArrayProduct = [];
    for (var bI = 0; bI < gDataProduct.length; bI++) {
        if (gDataProduct[bI].productName.indexOf(paramString) !== -1) {
            vArrayProduct.push(gDataProduct[bI]);
        }
    }
    return vArrayProduct;
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onFillData(paramData) {
    for (var bI = 0; bI < paramData.length; bI++) {
        $("#row-product-all").append(`
                <div class="col-sm-4 col-6">
                   
                    <a href="http://localhost:5500/DetailProduct.html?id-product=${paramData[bI].id}">
                        <img src="./image/${paramData[bI].photo}" alt="" width="90%">
                    </a>

                    <div class="row mt-4">
                        <div class="col-sm-12 text-center">
                            <p class="product-name">${paramData[bI].productName}</p>
                                <div class="d-flex justify-content-center">
                                    <p class="price-product-decoration mr-2">$ ${paramData[bI].buyPrice}</p>
                                    <p class="price-product">$ ${paramData[bI].priceSale}</p>
                                </div>
                            </div>
                        </div>


                    </div>
            `)
    }
}


