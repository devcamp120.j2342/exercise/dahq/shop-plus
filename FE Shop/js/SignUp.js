
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/"

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    $("#btn-signup").on("click", function () {
        onBtnSignUp();
        // onBtnCreateCustomer();
    })

    // var token = localStorage.getItem("token");
    // console.log("helloo" + token)
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

function onBtnSignUp() {
    var vConfimPass = "";
    var vObj = {
        username: "",
        password: "",
        email: "",

    }
    vObj.username = $("#inp-username").val();
    vObj.password = $("#inp-password").val();
    vObj.email = $("#inp-email").val();
    var vConfimPass = $("#inp-password-confirm").val();
    console.log(vObj);
    if (vConfimPass == vObj.password) {
        $.ajax({
            url: "http://localhost:8080/api/auth/signup",
            type: "POST",
            data: JSON.stringify(vObj),
            datatype: "application/json",
            contentType: "application/json; charset=utf-8",
            success: function (paramRes) {

                // localStorage.setItem('user_id', paramRes.id);
                // localStorage.setItem('token', paramRes.accessToken);
                // var token = paramRes.accessToken;
                var url = "http://localhost:5500/Login.html";
                window.location.href = url;


                // console.log(paramRes.accessToken);
            },
            error: function (paramErr) {
                alert(paramErr.status);
            }

        })
    }
    else {
        alert("password khong trung khop");
    }

}
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

