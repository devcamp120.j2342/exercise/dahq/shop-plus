package com.devcamp.shopplus.Entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "order_details")
public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "quantity_order")
    private int quantityOrder;
    @Column(name = "price_each")
    private double priceEach;
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    @Column(name = "product_cart_id")
    private long productCartId;
    @Column(name = "customer_cart_id")
    private long customerCartId;
    private String photo;
    private String productName;
    private double buyPrice;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantityOrder() {
        return quantityOrder;
    }

    public void setQuantityOrder(int quantityOrder) {
        this.quantityOrder = quantityOrder;
    }

    public double getPriceEach() {
        return priceEach;
    }

    public void setPriceEach(double peiceEach) {
        this.priceEach = peiceEach;
    }

    // public Order getOrder() {
    // return order;
    // }

    public void setOrder(Order order) {
        this.order = order;
    }

    // public List<Product> getProduct() {
    // return product;
    // }

    public void setProduct(Product product) {
        this.product = product;
    }

    public long getProductCartId() {
        return productCartId;
    }

    public void setProductCartId(long productCartId) {
        this.productCartId = productCartId;
    }

    public long getCustomerCartId() {
        return customerCartId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public void setCustomerCartId(long customerCartId) {
        this.customerCartId = customerCartId;
    }

    public OrderDetail(long id, int quantityOrder, double priceEach, Order order, Product product) {
        this.id = id;
        this.quantityOrder = quantityOrder;
        this.priceEach = priceEach;
        this.order = order;

    }

    public OrderDetail() {
    }

}
