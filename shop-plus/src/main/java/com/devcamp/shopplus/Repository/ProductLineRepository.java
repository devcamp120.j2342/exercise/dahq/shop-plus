package com.devcamp.shopplus.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.Product;
import com.devcamp.shopplus.Entity.ProductLine;
import java.util.List;

public interface ProductLineRepository extends JpaRepository<ProductLine, Long> {

}
