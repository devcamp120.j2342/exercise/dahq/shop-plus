
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var productId = localStorage.getItem('productId');
const gBASE_URL = "http://localhost:8080/"
var accessToken = localStorage.getItem('token');
var vUserId = localStorage.getItem('user_id');
var vCustomerId = 0;
var vAmount = 0;


/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onLoadCart();
    $("#table-cart").on("click", ".minus", function () {
        var vId = parseInt($(this).closest('tr').find('td:nth-child(3)').text())
        var vMinus = ($(this).closest('tr').find('td:nth-child(7)').text())
        $(this).closest('tr').find('td:nth-child(7)').text(parseInt(vMinus - 1));
        onBtnUpdateQuantity(vId, vMinus - 1);
        // onBtnUpdateQuantityFollowOrder(vId, vMinus - 1);
    })
    $("#table-cart").on("click", ".plus", function () {
        var vId = parseInt($(this).closest('tr').find('td:nth-child(3)').text())
        var vPlus = parseInt($(this).closest('tr').find('td:nth-child(7)').text())

        $(this).closest('tr').find('td:nth-child(7)').text((vPlus + 1));
        onBtnUpdateQuantity(vId, vPlus + 1);
        // onBtnUpdateQuantityFollowOrder(vId, vPlus + 1);
    })
    $("#table-cart").on("click", ".btn-delete", function () {
        var vId = parseInt($(this).closest('tr').find('td:nth-child(3)').text())
        onBtnDeleteCart(vId);
    })
    $("#btn-payment").on("click", function () {
        onBtnAddOrder();
        alert("You need to provide complete information before making a purchase.");
        window.location.href = "http://localhost:5500/User.html"

    })


})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onLoadCart() {
    $.ajax({
        url: gBASE_URL + "cart/" + vUserId,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            fillDataTableCart(paramRes);
            vCustomerId = paramRes[0].user.customer.id;
            localStorage.setItem('customer_id', paramRes[0].user.customer.id);
            vAmount = handleTotalBill(paramRes);
        },

        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}


function onBtnUpdateQuantity(vId, vQuantity) {
    var vObj = {
        quantity: 0,
    }
    vObj.quantity = vQuantity;
    console.log(vObj.quantity);
    $.ajax({
        url: gBASE_URL + "cart/" + vId,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),

        success: function (paramRes) {
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })

}

function onBtnDeleteCart(vId) {
    $.ajax({
        url: gBASE_URL + "cart/" + vId,
        type: "DELETE",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            alert("delete success");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
function onBtnAddOrder() {
    var vObj = {
        comments: "",
        // OrderDate: "",
        // requiredDate: "",
        // shippedDate: "",
        totalBill: vAmount,
        status: "Đơn hàng đang được giao",

    }

    $.ajax({
        url: gBASE_URL + "order/" + vCustomerId,
        type: "POST",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),
        success: function (paramRes) {
            console.log("add order success");
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })

}
function handleTotalBill(paramData) {
    var vCount = 0;
    for (var bI = 0; bI < paramData.length; bI++) {
        vCount += paramData[bI].totalBill;
    }
    return vCount;
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function fillDataTableCart(paramData) {


    for (var bI = 0; bI < paramData.length; bI++) {
        $("#tr-cart").append(`
            <tr>
                <td>
                    <input type="checkbox" name="" id="" class="filter-check">
                </td>
                <td class="">${bI + 1}</td>
                <td>${paramData[bI].id}</td>
                <td>${paramData[bI].product.productName}</td>
                <td><img src="./image/${paramData[bI].product.photo}" alt="" width="100px"></td>
                <td style="text-align: right;">
                    <i id="minus-count" class="fas fa-minus-circle fa-lg minus"></i>
                </td>
                <td class="" style="text-align: center;">
                        <p id="count" class="count-detail">${paramData[bI].quantity}</p>
                        
                </td>
                <td >
                    <i id="plus-count" class="fas fa-plus-circle fa-lg plus "></i>
                </td>
                <td>${paramData[bI].product.buyPrice}</td>
                <td>${paramData[bI].totalBill}</td>
                <td><i class="fa-solid fa-trash btn-delete" style="color: #1818e2;"></i></td>
                </tr>
            `)
    }
}




