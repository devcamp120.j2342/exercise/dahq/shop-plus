package com.devcamp.shopplus.Controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.Product;
import com.devcamp.shopplus.Entity.ReviewProduct;
import com.devcamp.shopplus.Model.User;
import com.devcamp.shopplus.Repository.ProductRepository;
import com.devcamp.shopplus.Repository.ReviewProductRepository;
import com.devcamp.shopplus.Repository.UserRepository;

@CrossOrigin
@RestController
@RequestMapping("/")

public class ReviewProductController {
    @Autowired
    ReviewProductRepository reviewProductRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    UserRepository userRepository;

    @GetMapping("/review")
    public List<ReviewProduct> getAllReview() {
        return reviewProductRepository.findAll();
    }

    @GetMapping("/review/{id}")
    public ReviewProduct getReviewById(@PathVariable("id") long id) {
        return reviewProductRepository.findById(id).get();
    }

    @PostMapping("/review/{idProduct}/{idUser}")
    public ResponseEntity<ReviewProduct> createReviewProduct(@PathVariable("idProduct") long idProduct,
            @PathVariable("idUser") long idUser, @RequestBody ReviewProduct pReviewProduct) {
        try {
            Optional<Product> productData = productRepository.findById(idProduct);
            Optional<User> userData = userRepository.findById(idUser);
            ReviewProduct reviewData = new ReviewProduct();
            reviewData.setRating(pReviewProduct.getRating());
            reviewData.setComment(pReviewProduct.getComment());
            reviewData.setCreateDate(new Date());
            reviewData.setProduct(productData.get());
            reviewData.setUser(userData.get());
            reviewData.setPhoto(pReviewProduct.getPhoto());
            reviewData.setUsername(pReviewProduct.getUsername());
            return new ResponseEntity<>(reviewProductRepository.save(reviewData), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/review/{id}")
    public ResponseEntity<ReviewProduct> deleteReview(@PathVariable("id") long id) {
        reviewProductRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
