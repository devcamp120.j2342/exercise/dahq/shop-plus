package com.devcamp.shopplus.security.services;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Repository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    public Map<String, Double> getCustomerTotal() {
        List<Object[]> customerTotal = customerRepository.getCustomerTotal();
        return convertToMap(customerTotal);
    }

    public Map<String, Double> convertToMap(List<Object[]> data) {
        Map<String, Double> resultMap = new HashMap<>();
        for (Object[] row : data) {
            String name = (String) row[0];
            Double total = (Double) row[1];
            resultMap.put(name, total);
        }
        return resultMap;
    }

    public Map<BigInteger, Double> convertToMApCustomerId(List<Object[]> data) {
        Map<BigInteger, Double> resulMap = new HashMap<>();
        for (Object[] row : data) {
            BigInteger id = (BigInteger) row[0];
            Double total = (Double) row[1];
            resulMap.put(id, total);
        }
        return resulMap;
    }

    public Map<BigInteger, Double> getIdCustomerTotal() {
        List<Object[]> listTotal = customerRepository.getIdCustomerTotal();
        return convertToMApCustomerId(listTotal);
    }
}
