package com.devcamp.shopplus.security.services;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.math.BigDecimal;
import java.sql.Date;

import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Repository.OrderRepository;

@Service

public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Map<LocalDate, Double> getDailyTotal() {
        List<Object[]> dailyTotals = orderRepository.getDailyTotal();
        return convertToMap(dailyTotals);
    }

    public Map<LocalDate, Double> getWeeklyTotal() {
        List<Object[]> weeklyTotals = orderRepository.getWeeklyTotal();
        return convertToMap(weeklyTotals);
    }

    private Map<LocalDate, Double> convertToMap(List<Object[]> data) {
        Map<LocalDate, Double> resultMap = new LinkedHashMap<>();
        for (Object[] row : data) {
            LocalDate date = ((Date) row[0]).toLocalDate();
            Double total = (Double) row[1];
            resultMap.put(date, total);
        }
        return resultMap;
    }
}
