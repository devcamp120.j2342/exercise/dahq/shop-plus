
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/"
var accessToken = localStorage.getItem('token');
var vUserId = localStorage.getItem('user_id');
var vCusId = localStorage.getItem('customer_id');






/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    console.log(vCusId + " cusis la")
    onLoadCustomer();
    $("#follow-order").on("click", ".btn-da-nhan-hang", function () {
        var vString = $(this).html();
        var idOrder = vString.substring(12, 15);
        var vIdOrder = parseInt(idOrder);
        onBtnUpdateOrder(vIdOrder);

    })
    $("#follow-order").on("click", ".btn-danh-gia", function () {

        var vString = $(this).html();
        var idOrder = vString.substring(68, 70);
        var vIdOrder = parseInt(idOrder);
        onLoadReviewOrder(vIdOrder);
        $("#modal-danh-gia").modal('show');
        $("#modal-body").html("");



    })

    $("#modal-danh-gia").on("click", ".btn-ok", function () {
        var vString = $(this).html();
        var vIdProStr = vString.substring(30, 31);
        var vIdPro = parseInt(vIdProStr);
        onBtnAddReviewProduct(vIdPro);
    })




})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */




function onLoadCustomer() {
    $.ajax({
        url: gBASE_URL + "customer/" + vCusId,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {

            console.log(paramRes);
            writeOrder(paramRes)
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}

function onBtnUpdateOrder(vIdOrder) {
    $.ajax({
        url: gBASE_URL + "order/updateStatus/" + vIdOrder,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            console.log("da nha hang thnha cong");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}


function onLoadReviewOrder(vIdOrder) {
    $.ajax({
        url: gBASE_URL + "order/" + vIdOrder,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {

            onFillModal(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}

function onBtnAddReviewProduct(vIdPro) {
    var vObj = {
        comment: "",
        rating: 0,
        photo: "",
        username: "",
    }
    getInfoReview(vObj);
    console.log(vObj);
    $.ajax({
        url: gBASE_URL + "review/" + vIdPro + "/" + vUserId,
        type: "POST",
        data: JSON.stringify(vObj),
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (paramRes) {
            alert("review thành công");
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })

}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onFillModal(paramData) {
    for (var bI = 0; bI < paramData.orderDetails.length; bI++) {
        $("#modal-body").append(`
            <div id="modal-body1-${bI}">
                <div class="d-flex">
                <img src="./image/${paramData.orderDetails[bI].photo}" alt="" width="60px">
                <p class="mt-3">${paramData.orderDetails[bI].productName}</p>
                
            </div>   
            </div>
            <div id="id-product-review">
                <p class="id-pro-re"></p>
            </div>
            <div class="">
                <h6>UserName</h6>
                <br>
                <input type="text" class="form-control mb-3" id="inp-username" placeholder="username">
                <h6>Đánh giá sản phẩm</h6>
                <br>
                <select class="form-control" id="select-rating">
                    <option value="5">Tuyệt vời
                    </option>
                    <option value="4">Hài lòng</option>
                    <option value="3">Bình thường</option>
                    <option value="2">Không hài lòng</option>
                    <option value="1">Tệ</option>
                </select>
            </div>
            
            <h6 class="mt-4 mr-3" for="">Image</h6>
            <br>
            <input type="file" name="" id="inp-file" class="">
            <br>
            <h6 class="mt-3">Comment</h6>
            <textarea id="inp-comment" name="" rows="4" cols="50" class="form-control"></textarea>
            <br>

            <button class="btn btn-success px-3 btn-ok">Ok <p style="display: none;"> ${paramData.orderDetails[bI].productCartId}</p></button>
            <hr class="my-3">
            
        `)


    }
}


function writeOrder(paramData) {

    for (var bI = 0; bI < paramData.orders.length; bI++) {
        $("#follow-order").append(`
                <div class="follow">
                     <p class="follow-status"><img src="./image/icons8-delivery-48.png" class="mr-2" alt="">
                     ${paramData.orders[bI].status}
                      </p>
                      <button id="" class="btn btn-success btn-da-nhan-hang">Đã nhận hàng ${paramData.orders[bI].id}</button>
                      <div id="order-detail-${bI}">
                        
                      </div>
                      <hr class="my-3">
                <div class="d-flex justify-content-end my-3">
                     <p><img src="./image/icons8-euro-money-48.png" alt=""><b>Thành Tiền: </b> <b
                         class="text-danger follow-price">$${paramData.orders[bI].totalBill}</b></p>
                 </div>
                      
                      <div class="row">
                    <div class="col-sm-4">
                         <div id="follow-order-required-date">
                             <p id="follow-order-required-date" >
                                 Đánh giá trước ngày 22/08/2023
                                 <p class="text-danger">Và nhận ngay xu</p>
                             </p>
                         </div>


                     </div>

                     <div id="btn-contact" class="col-md-8 col-12 text-right">
                         <div class="d-flex justify-content-end">
                             <button class="btn btn-danh-gia-reponsive  px-5 mr-4 btn-order-bot btn-danh-gia" style="background-color: #3b78ff;">Đánh
                                 giá <p style="display: none;">${paramData.orders[bI].id}</p></button>
                             <button class="btn px-3 mr-4 btn-order-bot" style="background-color: #c0bfbf;">Liên hệ người
                                 bán</button>
                             <button class="btn px-5 btn-order-bot" style="background-color: #c0bfbf;">Mua lại</button>
                         </div>
                     </div>
                 </div >

                      
                 </div>
             `)
        for (var j = 0; j < paramData.orders[bI].orderDetails.length; j++) {

            $(`#order-detail-${bI}`).append(`
                       
                      
                <div class="d-flex">
                <div class="p-2">
                     <img class="img-follow-headphone mt-3" src="./image/${paramData.orders[bI].orderDetails[j].photo}" width="120px"
                         alt="">
                 </div>
                 <div class="p-2 ml-4">
                     <div class="">
                         <p>${paramData.orders[bI].orderDetails[j].productName}</p>
                         <p>x${paramData.orders[bI].orderDetails[j].quantityOrder}</p>
                         <p class="follow-7day">
                             7 Ngày đổi trả
                             <p>
                             </div>
                     </div>
                     <div class="ml-auto p-2 mt-5 d-flex buy-price-responsive">
                         <p class="follow-price mr-3 follow-price-deco">$12345</p>
                         <b class="text-danger follow-price">$${paramData.orders[bI].orderDetails[j].buyPrice}</b>
                     </div>
                 </div>
                                         
                                 
                      `)


        }


    }

}
function getInfoReview(paramObj) {
    paramObj.comment = $("#inp-comment").val();
    paramObj.rating = $("#select-rating").val();
    paramObj.username = $("#inp-username").val();
    var filePath = $("#inp-file").val();
    var fileName = filePath.replace(/^.*\\/, "");
    paramObj.photo = fileName;
}



