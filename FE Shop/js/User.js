
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/"
var accessToken = localStorage.getItem('token');
var vUserId = localStorage.getItem('user_id');
var vCustomerId = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onLoadUser();
    // onCreateCustomerByUserId();
    onLoadInfoUser();
    $("#btn-save-info").on("click", function () {
        onBtnUpdateCustomer(vCustomerId);
    })
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onLoadUser() {
    $.ajax({
        url: gBASE_URL + "users/allUsers",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {

            console.log(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
function onCreateCustomerByUserId() {
    var vObj = {
        lastName: null,
        firstName: null,
        phoneNumber: null,
        address: null,
        city: null,
        state: null,
        postalCode: null,
        country: null,
        saleRepEmployeeNumber: 0,
        creditLimit: 0,
        payments: [],
        orders: []
    }
    $.ajax({
        url: gBASE_URL + "customer/" + vUserId,
        type: "POST",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),

        success: function (paramRes) {
            alert("success");
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadInfoUser() {
    $.ajax({
        url: gBASE_URL + "users/user/" + vUserId,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {

            console.log(paramRes);
            handleLoadData(paramRes);
            vCustomerId = paramRes.customer.id;
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
function onBtnUpdateCustomer(vIdCustomer) {
    var vObj = {
        lastName: null,
        firstName: null,
        phoneNumber: null,
        address: null,
        city: null,
        state: null,
        postalCode: null,
        country: null,
        saleRepEmployeeNumber: 0,
        creditLimit: 0,
        payments: [],
        orders: []
    }

    thuThapThongTinInforCustomer(vObj);
    $.ajax({
        url: gBASE_URL + "customer/" + vIdCustomer,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),

        success: function (paramRes) {
            alert("success");
            window.location.href = "http://localhost:5500/Payment.html"
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })

}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function handleLoadData(paramData) {
    $("#inp-username").val(paramData.username);
    $("#inp-fname").val(paramData.customer.firstName);
    $("#inp-lname").val(paramData.customer.lastName);
    $("#inp-email").val(paramData.email);
    $("#inp-phone").val(paramData.customer.phoneNumber);
    $("#inp-address").val(paramData.customer.address);

}
function thuThapThongTinInforCustomer(paramObj) {
    paramObj.lastName = $("#inp-lname").val();
    paramObj.firstName = $("#inp-fname").val();
    paramObj.phoneNumber = $("#inp-phone").val();
    paramObj.address = $("#inp-address").val();
}
