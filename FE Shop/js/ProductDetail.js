


"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/"
var accessToken = localStorage.getItem('token');
var vUserId = localStorage.getItem('user_id');
var urlParams = new URLSearchParams(window.location.search);
var vIdProduct = urlParams.get('id-product');
localStorage.setItem('productId', vIdProduct);
console.log(vIdProduct);
console.log(accessToken);

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    // 
    document.getElementById("read-more-btn").addEventListener("click", function () {
        var textContainer = document.getElementById("text-container");
        textContainer.style.height = "auto";
        textContainer.style.overflow = "visible";
        textContainer.classList.add("expanded");
        this.style.display = "none";
    });
    var vCount = $("#count").html();
    $("#minus-count").on("click", function () {
        if (vCount > 0) {
            $("#count").html(parseInt(vCount--))
        }

    })
    $("#plus-count").on("click", function () {
        $("#count").html(parseInt(vCount++))
    })
    // 

    onLoadProductDetail();

    $("#btn-add-cart").on("click", function () {
        onBtnAddProductCart();
        onBtnAddCartFollow();
        $("#continute").on("click", function () {
            $("#modal-add-cart").modal('hide');
            alert("Great! Please feel free to browse our collection of products. If you have any questions or need assistance, feel free to ask. Happy shopping!")

        })
        $("#shopping-cart").on("click", function () {
            window.location.href = "http://localhost:5500/Cart.html";
        })

    })



})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onLoadProductDetail() {
    $.ajax({
        url: gBASE_URL + "product/" + vIdProduct,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            onFillData(paramRes);
            console.log(paramRes);
            onFillReviewProduct(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}


function onFillData(paramData) {
    $("#img-product-detail").append(`
            <img src="./image/${paramData.photo}" width="70%" alt="">
                <div class="d-flex">
                    <img class="img-detail-more" src="./image/${paramData.photo}" width="25%" alt="">
                    <img class="img-detail-more" src="./image/${paramData.photo}" width="25%" alt="">
                    <img class="img-detail-more" src="./image/${paramData.photo}" width="25%" alt="">
                </div>
        `)
}
function onBtnAddProductCart() {
    $.ajax({
        url: gBASE_URL + "addToCart/" + vIdProduct,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            $("#modal-add-cart").modal('show');
            // alert("add success");

        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
// fuunction add cart follow

function onBtnAddCartFollow() {
    $.ajax({
        url: gBASE_URL + "addToCartFollow/" + vIdProduct,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            console.log("add foollow success");

        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onFillReviewProduct(paramData) {

    for (var bI = 0; bI < paramData.reviewProducts.length; bI++) {
        $("#detail-product-review").append(`
            <div class="border-review-product">
            
            <div class="d-flex">
                <img src="./image/icons8-user-100.png" alt="">
                <div>
                    <p>${paramData.reviewProducts[bI].username}</p>
                    <div>${paramData.reviewProducts[bI].createDate}</div>
                </div>

            </div>
            <div id="load-rating-review-${bI}">
                
            </div>
            <div class="d-flex mt-3">
                <p class="mr-3">Màu sắc:</p>
                <p>${paramData.color}</p>
            </div>
            
           
            <p>${paramData.reviewProducts[bI].comment}</p>
            <div class="d-flex">
                <img class="mr-3" src="./image/${paramData.reviewProducts[bI].photo}" alt="" width="250px">
                
            </div>
            </div>
        `)

        if (paramData.reviewProducts[bI].rating == 5) {
            $(`#load-rating-review-${bI}`).append(`
                <div class="d-flex mt-3">
                    <img class="mr-2" src="./image/star.png" width="20px" alt="">
                    <img class="mr-2" src="./image/star.png" width="23px" alt="">
                    <img class="mr-2" src="./image/star.png" width="23px" alt="">
                    <img class="mr-2" src="./image/star.png" width="23px" alt="">
                    <img class="mr-2" src="./image/star.png" width="23px" alt="">

                </div>
                `)
        }
        else if (paramData.reviewProducts[bI].rating == 4) {
            $(`#load-rating-review-${bI}`).append(`
                <div class="d-flex mt-3">
                    <img class="mr-2" src="./image/star.png" width="20px" alt="">
                    <img class="mr-2" src="./image/star.png" width="23px" alt="">
                    <img class="mr-2" src="./image/star.png" width="23px" alt="">
                    <img class="mr-2" src="./image/star.png" width="23px" alt="">
                    <img class="mr-2" src="./image/star (1).png" width="23px" alt="">
                </div>
                `)
        }
        else if (paramData.reviewProducts[bI].rating == 3) {
            $(`#load-rating-review-${bI}`).append(`
                <div class="d-flex mt-3">
                    <img class="mr-2" src="./image/star.png" width="20px" alt="">
                    <img class="mr-2" src="./image/star.png" width="23px" alt="">
                    <img class="mr-2" src="./image/star.png" width="23px" alt="">
                    <img class="mr-2" src="./image/star (1).png" width="23px" alt="">
                    <img class="mr-2" src="./image/star (1).png" width="23px" alt="">
                </div>
                `)
        }
        else if (paramData.reviewProducts[bI].rating == 2) {
            $(`#load-rating-review-${bI}`).append(`
                <div class="d-flex mt-3">
                    <img class="mr-2" src="./image/star.png" width="20px" alt="">
                    <img class="mr-2" src="./image/star.png" width="23px" alt="">
                    <img class="mr-2" src="./image/star (1).png" width="23px" alt="">
                    <img class="mr-2" src="./image/star (1).png" width="23px" alt="">
                    <img class="mr-2" src="./image/star (1).png" width="23px" alt="">
                </div>
                `)
        }
        else if (paramData.reviewProducts[bI].rating == 1) {
            $(`#load-rating-review-${bI}`).append(`
                <div class="d-flex mt-3">
                    <img class="mr-2" src="./image/star.png" width="20px" alt="">
                    <img class="mr-2" src="./image/star (1).png" width="23px" alt="">
                    <img class="mr-2" src="./image/star (1).png" width="23px" alt="">
                    <img class="mr-2" src="./image/star (1).png" width="23px" alt="">
                    <img class="mr-2" src="./image/star (1).png" width="23px" alt="">
                </div>
                `)
        }

    }

}

