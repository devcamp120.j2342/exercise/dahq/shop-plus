
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var accessToken = localStorage.getItem('token');
var userId = localStorage.getItem('user_id');

$(document).ready(function () {
    $("#btn-login").on("click", function () {
        onBtnLogin();

    })

    $("#forgot-pass").on("click", function () {

    })


})
function onBtnLogin() {
    var vObj = {
        username: "",
        password: "",
    }
    vObj.username = $("#inp-username").val();
    vObj.password = $("#inp-password").val();
    console.log(vObj);
    $.ajax({
        url: "http://localhost:8080/api/auth/signin",
        type: "POST",
        data: JSON.stringify(vObj),
        datatype: "application/json",
        contentType: "application/json; charset=utf-8",
        success: function (paramRes) {


            localStorage.setItem('user_id', paramRes.id);
            localStorage.setItem('token', paramRes.accessToken);
            var url = "http://localhost:5500/Home.html";
            window.location.href = url;
            onLoadAllUser(paramRes);

            console.log(paramRes.accessToken);

        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
function onBtnCreateCustomer(paramData) {
    var vObj = {
        firstname: "",
        lastname: "",
        phoneNumber: "",
        address: "",
        city: "",
        state: "",
        postalCode: "",
        country: "Viet Nam",
        creditLimit: 0,
        saleRepEmployeeNumber: 0,
    }
    $.ajax({
        url: "http://localhost:8080/customer/" + paramData.id,
        type: "POST",
        headers: {
            "Authorization": "Bearer " + paramData.accessToken,
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),
        success: function (paramRes) {
            console.log("add customer success");
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadAllUser(paramData) {
    $.ajax({
        url: "http://localhost:8080/users/user/" + paramData.id,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + paramData.accessToken,
        },
        success: function (paramRes) {

            console.log(paramRes);
            if (paramRes.customer === null) {
                onBtnCreateCustomer(paramData, paramData);
            }

        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })

}
function forgotPassword() {

}

