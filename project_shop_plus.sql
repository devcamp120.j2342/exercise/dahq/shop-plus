-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 06, 2023 at 05:35 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_shop_plus`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `quantity` bigint(20) NOT NULL,
  `total_bill` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `product_id`, `user_id`, `quantity`, `total_bill`) VALUES
(30, 4, 5, 10, 1200),
(31, 2, 7, 6, 3000),
(32, 5, 7, 2, 240),
(34, 5, 8, 6, 720),
(35, 4, 8, 1, 120),
(37, 3, 5, 4, 1200),
(38, 4, 7, 2, 240),
(39, 3, 8, 2, 600),
(40, 3, 26, 7, 2100),
(41, 5, 28, 7, 840),
(42, 3, 28, 3, 900),
(43, 3, 27, 5, 1500),
(47, 5, 25, 2, 240),
(52, 27, 24, 20, 13600),
(55, 19, 5, 1, 270),
(56, 17, 25, 1, 660);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `credit_limit` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `sale_rep_employee_number` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `address`, `city`, `country`, `credit_limit`, `first_name`, `last_name`, `phone_number`, `postal_code`, `sale_rep_employee_number`, `state`, `user_id`) VALUES
(3, 'ThuanHung-LongMy-CanTho', NULL, NULL, 0, 'Huynh', 'Quang Da', '078998767', NULL, 0, NULL, 5),
(6, 'Hon Dat-Kien Giang', 'Kien Giang', 'Viet Nam', 10000000, 'Nguyen ', 'Nhat Anh', '1234567123', NULL, 0, NULL, 8),
(7, 'Long My Can tho', NULL, NULL, 0, 'Nguyen Van A', 'Van A', '987652108', NULL, 0, NULL, 9),
(8, 'Vinh long', NULL, NULL, 0, 'Huynh ', 'Phu Quy', '1234567789', NULL, 0, NULL, 7),
(9, 'My Hoa-Long Xuyen-An giang', NULL, NULL, 0, 'Nguyen', 'Nhat Minh', '078996589', NULL, 0, NULL, 26),
(14, 'Thuan Hung-Long my-Hau Giang', NULL, NULL, 0, 'Nguyen Tran', 'Bao Ngoc', '1233324567', NULL, 0, NULL, 28),
(15, 'An Hoa-Ninh Kieu-Can Tho', NULL, NULL, 0, 'Pham', 'Quoc Nhat', '1233345567', NULL, 0, NULL, 24),
(16, 'Thuan Hung-Long My-Hau Giang', NULL, NULL, 0, 'Tran ', 'Tri Nguyen', '089976552', NULL, 0, NULL, 25),
(17, 'Vinh Thuan Dong-Long My- Hau Giang', NULL, NULL, 0, 'Huynh', 'Anh Thi', '1111112345', NULL, 0, NULL, 27);

-- --------------------------------------------------------

--
-- Table structure for table `demo`
--

CREATE TABLE `demo` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `office_code` bigint(20) DEFAULT NULL,
  `report_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `follow_order`
--

CREATE TABLE `follow_order` (
  `id` bigint(20) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `total_bill` double DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `follow_order`
--

INSERT INTO `follow_order` (`id`, `quantity`, `total_bill`, `product_id`, `user_id`) VALUES
(17, 12, 6000, 2, 5),
(18, 9, 2700, 3, 5),
(19, 7, 840, 4, 5),
(20, 11, 1320, 5, 5),
(21, 1, 500, 2, 7),
(22, 1, 120, 5, 7),
(23, 2, 600, 3, 8),
(24, 1, 120, 5, 8),
(25, 1, 120, 4, 8),
(26, 1, 300, 3, 9),
(27, 1, 120, 4, 7),
(28, 1, 300, 3, 26),
(29, 1, 120, 5, 28),
(30, 1, 300, 3, 28),
(31, 1, 300, 3, 27),
(32, 1, 500, 2, 27),
(33, 2, 240, 4, 25),
(34, 1, 500, 2, 25),
(35, 2, 240, 5, 25),
(36, 1, 500, 2, 24),
(37, 1, 120, 4, 24),
(38, 1, 300, 3, 24),
(39, 1, 400, 10, 24),
(40, 1, 680, 27, 24),
(41, 1, 350, 6, 25),
(42, 1, 500, 2, 9),
(43, 1, 270, 19, 5),
(44, 1, 660, 17, 25);

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` bigint(20) NOT NULL,
  `address_line` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `territory` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `required_date` datetime DEFAULT NULL,
  `shipped_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `total_bill` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `comments`, `order_date`, `required_date`, `shipped_date`, `status`, `customer_id`, `total_bill`) VALUES
(46, '', '2023-08-24 18:48:48', '2023-08-26 18:48:48', '2023-08-26 18:48:48', 'Đã nhận hàng', 3, 1340),
(47, NULL, '2023-08-25 17:24:29', '2023-08-27 17:24:29', '2023-08-27 17:24:29', 'Đã nhận hàng', 3, 360),
(48, '', '2023-08-27 12:55:40', '2023-08-29 12:55:40', '2023-08-29 12:55:40', 'Đơn hàng đang được giao', 6, 1200),
(49, '', '2023-08-27 12:57:21', '2023-08-29 12:57:21', '2023-08-29 12:57:21', 'Đơn hàng đang được giao', 6, 720),
(50, '', '2023-08-27 14:16:55', '2023-08-29 14:16:55', '2023-08-29 14:16:55', 'Đơn hàng đang được giao', 3, 840),
(51, '', '2023-08-27 14:19:38', '2023-08-29 14:19:38', '2023-08-29 14:19:38', 'Đơn hàng đang được giao', 7, 1500),
(52, '', '2023-08-27 23:23:08', '2023-08-29 23:23:08', '2023-08-29 23:23:08', 'Đã nhận hàng', 3, 2940),
(53, '', '2023-08-27 23:26:19', '2023-08-29 23:26:19', '2023-08-29 23:26:19', 'Đơn hàng đang được giao', 8, 3480),
(54, '', '2023-08-27 23:28:48', '2023-08-29 23:28:48', '2023-08-29 23:28:48', 'Đơn hàng đang được giao', 6, 1440),
(55, '', '2023-08-28 13:58:14', '2023-08-30 13:58:14', '2023-08-30 13:58:14', 'Đơn hàng đang được giao', 9, 2100),
(56, '', '2023-08-28 14:47:54', '2023-08-30 14:47:54', '2023-08-30 14:47:54', 'Đơn hàng đang được giao', 14, 1740),
(57, '', '2023-08-28 15:00:17', '2023-08-30 15:00:17', '2023-08-30 15:00:17', 'Đơn hàng đang được giao', 17, 3500),
(58, '', '2023-08-28 15:02:31', '2023-08-30 15:02:31', '2023-08-30 15:02:31', 'Đơn hàng đang được giao', 17, 3500),
(59, '', '2023-08-28 15:11:07', '2023-08-30 15:11:07', '2023-08-30 15:11:07', 'Đơn hàng đang được giao', 17, 1500),
(60, '', '2023-08-28 15:18:20', '2023-08-30 15:18:20', '2023-08-30 15:18:20', 'Đã nhận hàng', 16, 360),
(61, '', '2023-08-28 15:19:06', '2023-08-30 15:19:06', '2023-08-30 15:19:06', 'Đơn hàng đang được giao', 16, 1980),
(62, '', '2023-08-28 20:22:08', '2023-08-30 20:22:08', '2023-08-30 20:22:08', 'Đơn hàng đang được giao', 16, 2220),
(63, '', '2023-08-28 20:53:57', '2023-08-30 20:53:57', '2023-08-30 20:53:57', 'Đơn hàng đang được giao', 16, 2220),
(64, '', '2023-08-29 09:45:23', '2023-08-31 09:45:23', '2023-08-31 09:45:23', 'Đơn hàng đang được giao', 15, 2420),
(65, '', '2023-08-29 11:43:24', '2023-08-31 11:43:24', '2023-08-31 11:43:24', 'Đơn hàng đang được giao', 15, 13600),
(67, NULL, '2023-08-29 15:56:03', '2023-08-31 15:56:03', '2023-08-31 15:56:03', '1111', 8, 0),
(68, '123 alo', '2023-08-29 15:59:32', '2023-08-31 15:59:32', '2023-08-31 15:59:32', 'Don hang dang giao', 7, 123),
(69, '', '2023-08-29 18:03:14', '2023-08-31 18:03:14', '2023-08-31 18:03:14', 'Đơn hàng đang được giao', 15, 13600),
(70, '', '2023-08-30 20:21:16', '2023-09-01 20:21:16', '2023-09-01 20:21:16', 'Đơn hàng đang được giao', 16, 2090),
(71, '', '2023-08-30 20:37:33', '2023-09-01 20:37:33', '2023-09-01 20:37:33', 'Đơn hàng đang được giao', 15, 13600),
(72, '', '2023-09-06 20:27:26', '2023-09-08 20:27:26', '2023-09-08 20:27:26', 'Đơn hàng đang được giao', 3, 3240),
(73, '', '2023-09-06 20:31:14', '2023-09-08 20:31:14', '2023-09-08 20:31:14', 'Đơn hàng đang được giao', 16, 900);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) NOT NULL,
  `price_each` double DEFAULT NULL,
  `quantity_order` int(11) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `product_cart_id` bigint(20) DEFAULT NULL,
  `customer_cart_id` bigint(20) DEFAULT NULL,
  `buy_price` double DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `price_each`, `quantity_order`, `order_id`, `product_id`, `product_cart_id`, `customer_cart_id`, `buy_price`, `photo`, `product_name`) VALUES
(25, 240, 2, 46, 5, 5, 3, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(26, 600, 2, 46, 3, 3, 3, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(27, 500, 1, 46, 2, 2, 3, 500, 'jbl bluetooth headphone T660 over.webp', 'JBL T660'),
(28, 240, 2, 47, 5, 5, 3, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(29, 120, 1, 47, 4, 4, 3, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(30, 600, 5, 48, 5, 5, 6, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(31, 600, 2, 48, 3, 3, 6, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(32, 600, 5, 49, 5, 5, 6, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(33, 120, 1, 49, 4, 4, 6, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(34, 120, 1, 50, 4, 4, 3, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(35, 720, 6, 50, 5, 5, 3, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(36, 1500, 5, 51, 3, 3, 7, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(37, 1200, 10, 52, 4, 4, 3, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(38, 840, 7, 52, 5, 5, 3, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(39, 900, 3, 52, 3, 3, 3, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(40, 240, 2, 53, 5, 5, 8, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(41, 240, 2, 53, 4, 4, 8, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(42, 3000, 6, 53, 2, 2, 8, 500, 'jbl bluetooth headphone T660 over.webp', 'JBL T660'),
(43, 120, 1, 54, 4, 4, 6, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(44, 600, 2, 54, 3, 3, 6, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(45, 720, 6, 54, 5, 5, 6, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(46, 2100, 7, 55, 3, 3, 9, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(47, 840, 7, 56, 5, 5, 14, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(48, 900, 3, 56, 3, 3, 14, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(49, 1500, 5, 57, 3, 3, 17, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(50, 2000, 4, 57, 2, 2, 17, 500, 'jbl bluetooth headphone T660 over.webp', 'JBL T660'),
(51, 1500, 5, 58, 3, 3, 17, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(52, 2000, 4, 58, 2, 2, 17, 500, 'jbl bluetooth headphone T660 over.webp', 'JBL T660'),
(53, 1500, 5, 59, 3, 3, 17, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(54, 1500, 3, 61, 2, 2, 16, 500, 'jbl bluetooth headphone T660 over.webp', 'JBL T660'),
(55, 120, 1, 61, 5, 5, 16, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(56, 360, 3, 61, 4, 4, 16, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(57, 1500, 3, 62, 2, 2, 16, 500, 'jbl bluetooth headphone T660 over.webp', 'JBL T660'),
(58, 480, 4, 62, 4, 4, 16, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(59, 240, 2, 62, 5, 5, 16, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(60, 1500, 3, 63, 2, 2, 16, 500, 'jbl bluetooth headphone T660 over.webp', 'JBL T660'),
(61, 480, 4, 63, 4, 4, 16, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(62, 240, 2, 63, 5, 5, 16, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(63, 120, 1, 64, 4, 4, 15, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(64, 2000, 4, 64, 2, 2, 15, 500, 'jbl bluetooth headphone T660 over.webp', 'JBL T660'),
(65, 300, 1, 64, 3, 3, 15, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(66, 13600, 20, 65, 27, 27, 15, 680, 'logistech over white G735.webp', 'Logistech G733'),
(67, 0, 0, 68, 1, 0, 0, 0, 'samsung in white buds2 pro.jpg', 'samsung buds'),
(68, 0, 0, 48, 2, 0, 0, 0, 'jbl bluetooth headphone T660 over.webp', 'JBL T660'),
(69, 0, 20, 68, 4, 0, 0, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(70, 50000, 100, 68, 8, 0, 0, 500, 'beat over white studio3.jpeg', 'Beat studio'),
(71, 13600, 20, 69, 27, 27, 15, 680, 'logistech over white G735.webp', 'Logistech G733'),
(72, 240, 2, 70, 5, 5, 16, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(73, 1500, 3, 70, 2, 2, 16, 500, 'jbl bluetooth headphone T660 over.webp', 'JBL T660'),
(74, 350, 1, 70, 6, 6, 16, 350, 'beat in pink beat fit pro.jpg', 'Beat pro'),
(75, 13600, 20, 71, 27, 27, 15, 680, 'logistech over white G735.webp', 'Logistech G733'),
(76, 1200, 4, 72, 3, 3, 3, 300, 'jbl-over.jpg', 'JBL 650 BTNC'),
(77, 1200, 10, 72, 4, 4, 3, 120, 'jbl in white Tune 225TWS.jpg', 'JBL White Tune 225TWS'),
(78, 840, 7, 72, 5, 5, 3, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS'),
(79, 660, 1, 73, 17, 17, 16, 660, 'samsung over blue level U.jpg', 'samsung sport'),
(80, 240, 2, 73, 5, 5, 16, 120, 'jbl in blue 100TWS.jpg', 'JBL Blue Tune 100TWS');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) NOT NULL,
  `ammount` double NOT NULL,
  `check_number` varchar(255) DEFAULT NULL,
  `pament_date` datetime DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) NOT NULL,
  `buy_price` double DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `price_sale` double DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `product_description` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_scale` varchar(255) DEFAULT NULL,
  `product_vendor` varchar(255) DEFAULT NULL,
  `quantity_in_stock` int(11) DEFAULT NULL,
  `rating` int(11) NOT NULL,
  `product_line_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `buy_price`, `color`, `photo`, `price_sale`, `product_code`, `product_description`, `product_name`, `product_scale`, `product_vendor`, `quantity_in_stock`, `rating`, `product_line_id`) VALUES
(1, 490, 'White', 'samsung in white buds2 pro.jpg', 400, 'samsung111', NULL, 'samsung buds', NULL, 'SAMSUNG', 100, 5, 3),
(2, 500, 'White', 'jbl bluetooth headphone T660 over.webp', 400, 'JBL111', NULL, 'JBL T660', NULL, 'JBL', 100, 5, 3),
(3, 300, 'Dark', 'jbl-over.jpg', 200, 'jbl112', NULL, 'JBL 650 BTNC', NULL, 'JBL', 50, 5, 3),
(4, 120, 'White', 'jbl in white Tune 225TWS.jpg', 100, 'jbl114', NULL, 'JBL White Tune 225TWS', NULL, 'JBL', 100, 4, 4),
(5, 120, 'Blue', 'jbl in blue 100TWS.jpg', 100, 'jbl115', NULL, 'JBL Blue Tune 100TWS', NULL, 'JBL', 100, 4, 4),
(6, 350, 'Pink', 'beat in pink beat fit pro.jpg', 300, 'Beat113', NULL, 'Beat pro', NULL, 'BEAT', 100, 4, 3),
(7, 400, 'Dark', 'beat sport dark Powerbeats pro.webp', 360, 'Beat114', NULL, 'Beat Power', NULL, 'BEAT', 100, 5, 5),
(8, 500, 'White', 'beat over white studio3.jpeg', 490, 'Beat115', NULL, 'Beat studio', NULL, 'BEAT', 100, 3, 4),
(9, 400, 'White', 'beat sport white beat power.jpg', 300, 'Beat116', NULL, 'Beat power', NULL, 'BEAT', 100, 4, 5),
(10, 400, 'Red', 'beat sport red powerbeat Earbuds.jpg', 300, 'Beat117', NULL, 'Beat power', NULL, 'BEAT', 100, 4, 5),
(11, 200, 'Blue', 'beat in blue beat fit pro.jpg', 190, 'Beat118', NULL, 'Beat fit', NULL, 'BEAT', 100, 3, 3),
(12, 600, 'White', 'beat in whte beats fit pro.jpg', 190, 'Beat118', NULL, 'Beat fit', NULL, 'BEAT', 100, 5, 3),
(13, 390, 'White', 'samsung in white buds 2 pro.webp', 200, 'samsung112', NULL, 'samsung buds', NULL, 'SAMSUNG', 100, 5, 3),
(14, 360, 'Dark', 'samsung in dark pro.jpg', 350, 'samsung113', NULL, 'samsung pro', NULL, 'SAMSUNG', 100, 3, 3),
(15, 560, 'Pink', 'samsung in  pink galaxy Buds Pro true.webp', 500, 'samsung114', NULL, 'samsung gxy', NULL, 'SAMSUNG', 100, 5, 3),
(16, 260, 'Dark', 'samsung over dark new help.jpg', 250, 'samsung115', NULL, 'samsung help', NULL, 'SAMSUNG', 100, 4, 4),
(17, 660, 'Blue', 'samsung over blue level U.jpg', 630, 'samsung116', NULL, 'samsung sport', NULL, 'SAMSUNG', 100, 4, 5),
(18, 200, 'White', 'sony in white wf-1000xm3.jpg', 190, 'sony111', NULL, 'sony wf1000', NULL, 'SONY', 100, 4, 3),
(19, 270, 'Dark', 'sony in dark wf-1000xm3.jpg', 190, 'sony112', NULL, 'sony wf1000', NULL, 'SONY', 100, 4, 3),
(20, 300, 'Dark', 'sony in dark wf-XB700.jpg', 290, 'sony113', NULL, 'sony xb700', NULL, 'SONY', 100, 5, 3),
(21, 400, 'Blue', 'sony in blue WF-XB700.jpg', 290, 'sony114', NULL, 'sony wf1000', NULL, 'SONY', 100, 5, 3),
(22, 300, 'White', 'sony over white WH-CH520.jpg', 290, 'sony115', NULL, 'sony ch520', NULL, 'SONY', 100, 3, 4),
(23, 400, 'Red', 'sony over red WH-H800.jpg', 300, 'sony116', NULL, 'sony h800', NULL, 'SONY', 100, 5, 4),
(24, 300, 'Red', 'logistech sport red jaybird x3.jpg', 280, 'logistech111', NULL, 'Logistech jay', NULL, 'LOGISTECH', 100, 4, 5),
(25, 370, 'Dark', 'logistech sport  dark jaybird x3.jpg', 330, 'logistech112', NULL, 'Logistech Djay', NULL, 'LOGISTECH', 100, 5, 5),
(26, 380, 'White', 'logistech in white fingerprint touch.jpg', 300, 'logistech113', NULL, 'Logistech Fin', NULL, 'LOGISTECH', 100, 4, 3),
(27, 680, 'White', 'logistech over white G735.webp', 660, 'logistech114', NULL, 'Logistech G733', NULL, 'LOGISTECH', 100, 3, 4),
(28, 580, 'Red', 'logistech over red G435.jpg', 560, 'logistech115', NULL, 'Logistech G435', NULL, 'LOGISTECH', 100, 5, 4),
(29, 0, '1', '1', 10, 'abbbcbc', 'a', 'aaaaa', NULL, 'a', 10, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `product_lines`
--

CREATE TABLE `product_lines` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `product_line` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_lines`
--

INSERT INTO `product_lines` (`id`, `description`, `product_line`) VALUES
(2, 'Tai nghe không dây', 'Wireless'),
(3, 'Tai nghe không dây headphone', 'Over Headphone'),
(4, 'Tai nghe không dây headphone', 'In ear Headphone'),
(5, 'Tai nghe không dây headphone', 'Sport Headphone');

-- --------------------------------------------------------

--
-- Table structure for table `review_product`
--

CREATE TABLE `review_product` (
  `id` bigint(20) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `rating` int(11) NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `review_product`
--

INSERT INTO `review_product` (`id`, `comment`, `create_date`, `rating`, `product_id`, `user_id`, `photo`, `username`) VALUES
(1, 'Sản phẩm chất lượng,giao hàng đúng thời gian,đúng mẫu,màu sắc', '2023-08-25 10:44:28', 5, 5, 5, 'lgo3.jpg', 'Dahuynh'),
(2, 'Sản phẩm tuyệt vời,,', '2023-08-25 10:45:54', 5, 3, 5, 'logo2.jpg', 'Dahuynh'),
(3, 'Sản phẩm tuyệt vời,,', '2023-08-25 10:46:35', 5, 5, 5, 'logo2.jpg', 'DaHuynh'),
(4, 'hello', '2023-08-28 15:09:42', 0, 2, 27, 'beat in blue beat fit pro.jpg', 'anhthi'),
(5, 'Sản phẩm rất tốt,,giao hàng cẩn thận', '2023-08-28 15:11:50', 5, 3, 27, 'beat over dark Beat Solo3.jpg', 'anh thi'),
(6, 'Sản phẩm tuyệt vời ông mặt trời', '2023-08-28 15:20:23', 5, 4, 25, 'beat in whte beats fit pro.jpg', 'tringuyen'),
(7, 'Sản phẩm tuyệt vời ông mặt trời', '2023-08-28 15:20:49', 5, 5, 25, 'beat in whte beats fit pro.jpg', 'tringuyen'),
(8, 'san pham tuyet voi,giao hang ky', '2023-08-29 09:46:22', 4, 3, 24, 'beat over dark Beat Solo3.jpg', 'quocnhat'),
(9, 'OK', '2023-09-06 20:28:43', 4, 3, 5, 'taophone4.jpg', 'tringuyen'),
(10, 'Sản phẩm rất tuyệt vời', '2023-09-06 20:31:57', 5, 1, 25, 'taiphone8.jpg', 'tringuyen');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'ROLE_USER'),
(2, 'ROLE_MODERATOR'),
(3, 'ROLE_ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `sub_photo`
--

CREATE TABLE `sub_photo` (
  `id` int(11) NOT NULL,
  `sub_photo` varchar(255) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `dtype` varchar(31) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_title` varchar(255) DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `username`, `dtype`, `address`, `contact_name`, `contact_title`, `create_by`, `create_date`, `mobile`, `note`, `update_by`, `update_date`) VALUES
(1, 'demo@devcamp.edu.vn', '$2a$10$CC6mE43iO/AKyEPTPiaL/.2kX7dDeik1QTeyxOT3EH5y1pLTzVnf6', 'DevcampUser', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'huynhquangda2016@gmail.com', '$2a$10$xLQd7uUHJdJNaO7Nf50hdOhZ7nED0C1k18sdR8ivtW3DaTSOkEh5C', 'DevcampUerrr', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'da@gmail.com', '$2a$10$PbKbfwECmMUABYqa8W2hquzDUY6sMs5gERtwixAHjQw9mCM0y8OxW', 'DaHuynh', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'abc@gmail.com', '$2a$10$sNoP4avKilUKhOeW4nxvOO6UQ09x6t3wGYjFRyBo3PeA7oLAdedbO', 'abc', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'java@gmail.com', '$2a$10$0Er72gGCiWoIBLU.2ZJzKuPml32Yst/GyKIKVvWs9fRZmiAGWJIT6', 'java', '', 'TP HCM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'python@gmail.com', '$2a$10$BE5DzAPhvb8jjMCFnTeFo.xuGAMcmqzIQ.IO6EH9YJXmW74ho/pPm', 'python', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'quy@gmail.com', '$2a$10$O3acejcSvppr2Fj7qUwA7e5WjHMkdxTexc.bEp58h9MFDkJD2Y9im', 'quy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'nhat@gmail.com', '$2a$10$UItItrBJwoYDVRwxr/JX7.HYX3Y6IaKvOEnuwsAlC7ISb9tavyCbq', 'quocnhat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'nguyen@gmail.com', '$2a$10$rSf7Hq62wJ2WPm4tcmvhzOiMP4lDg/APrTPWxsNbHlRbmAx/QpUPO', 'tringuyen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'nhatminh@gmail.com', '$2a$10$So5voXMuYojiCsv8.fLihegfdZ2gBgiUsYjiF0xzXxrRhqNB8Y752', 'nhatminh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'anhthi@gmail.com', '$2a$10$OLYRMSjZeN9eKloCjUCC4eEq8fJ74UJY/RxqLswoHMnbx52UCvdT2', 'anhthi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'baongoc@gmail.com', '$2a$10$KHr2IzFx1AWmVTkdmvctbOBNPhEgYXcgpWoN8e2ReItQHD3VsuR82', 'baongoc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(5, 1),
(7, 3),
(8, 1),
(9, 1),
(12, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKmd2ap4oxo3wvgkf4fnaye532i` (`product_id`),
  ADD KEY `FKb5o626f86h46m4s7ms6ginnop` (`user_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKrh1g1a20omjmn6kurd35o3eit` (`user_id`);

--
-- Indexes for table `demo`
--
ALTER TABLE `demo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follow_order`
--
ALTER TABLE `follow_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK21y2473kysg4q4rp3r22h4k04` (`product_id`),
  ADD KEY `FKblv3haw7gupe10e0r6q052lh0` (`user_id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKpxtb8awmi0dk6smoh2vp1litg` (`customer_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKjyu2qbqt8gnvno9oe9j2s2ldk` (`order_id`),
  ADD KEY `FK4q98utpd73imf4yhttm3w0eax` (`product_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK45dp0030s8e3myd8n6ky4e79g` (`customer_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1eicg1yvaxh1gqdp2lsda7vlv` (`product_line_id`);

--
-- Indexes for table `product_lines`
--
ALTER TABLE `product_lines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review_product`
--
ALTER TABLE `review_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK5c5xv8xb26eh3egs2ialh4apf` (`product_id`),
  ADD KEY `FKim1sjrjw0em3jvgnhgpo3xqgx` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_photo`
--
ALTER TABLE `sub_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK690a95t1mhn81cjb7ts7mrmcm` (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UKr43af9ap4edm43mmtq01oddj6` (`username`),
  ADD UNIQUE KEY `UK6dotkott2kjsp8vw4d0m25fb7` (`email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `FKh8ciramu9cc9q3qcqiv4ue8a6` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `demo`
--
ALTER TABLE `demo`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `follow_order`
--
ALTER TABLE `follow_order`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `product_lines`
--
ALTER TABLE `product_lines`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `review_product`
--
ALTER TABLE `review_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sub_photo`
--
ALTER TABLE `sub_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `FKb5o626f86h46m4s7ms6ginnop` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FKmd2ap4oxo3wvgkf4fnaye532i` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `FKrh1g1a20omjmn6kurd35o3eit` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `follow_order`
--
ALTER TABLE `follow_order`
  ADD CONSTRAINT `FK21y2473kysg4q4rp3r22h4k04` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FKblv3haw7gupe10e0r6q052lh0` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FKpxtb8awmi0dk6smoh2vp1litg` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `FK4q98utpd73imf4yhttm3w0eax` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FKjyu2qbqt8gnvno9oe9j2s2ldk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `FK45dp0030s8e3myd8n6ky4e79g` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK1eicg1yvaxh1gqdp2lsda7vlv` FOREIGN KEY (`product_line_id`) REFERENCES `product_lines` (`id`);

--
-- Constraints for table `review_product`
--
ALTER TABLE `review_product`
  ADD CONSTRAINT `FK5c5xv8xb26eh3egs2ialh4apf` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FKim1sjrjw0em3jvgnhgpo3xqgx` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `sub_photo`
--
ALTER TABLE `sub_photo`
  ADD CONSTRAINT `FK690a95t1mhn81cjb7ts7mrmcm` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `FKh8ciramu9cc9q3qcqiv4ue8a6` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `FKhfh9dx7w3ubf1co1vdev94g3f` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
