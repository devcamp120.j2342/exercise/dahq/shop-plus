package com.devcamp.shopplus.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.ReviewProduct;

public interface ReviewProductRepository extends JpaRepository<ReviewProduct, Long> {

}
