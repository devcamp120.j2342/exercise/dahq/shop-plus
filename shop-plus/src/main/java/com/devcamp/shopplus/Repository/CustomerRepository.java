package com.devcamp.shopplus.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.shopplus.Entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query(value = "SELECT CONCAT(c.first_name, ' ', c.last_name) AS full_name, SUM(o.total_bill) AS total_amount FROM customers c JOIN orders o ON c.id = o.customer_id GROUP BY c.id, full_name;", nativeQuery = true)
    List<Object[]> getCustomerTotal();

    @Query(value = "SELECT c.id, SUM(o.total_bill) AS total_amount FROM customers c JOIN orders o ON c.id = o.customer_id GROUP BY c.id", nativeQuery = true)
    List<Object[]> getIdCustomerTotal();
}
