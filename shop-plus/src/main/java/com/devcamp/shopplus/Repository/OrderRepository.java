package com.devcamp.shopplus.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.shopplus.Entity.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByCustomerId(long id);

    @Query(value = "SELECT DATE(order_date), SUM(total_bill) FROM orders GROUP BY DATE(order_date)", nativeQuery = true)
    List<Object[]> getDailyTotal();

    @Query(value = "SELECT DATE_TRUNC('week', order_date), SUM(total_bill) FROM orders GROUP BY DATE_TRUNC('week', order_date)", nativeQuery = true)
    List<Object[]> getWeeklyTotal();

}
