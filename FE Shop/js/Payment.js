
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/"
var accessToken = localStorage.getItem('token');
var vUserId = localStorage.getItem('user_id');

var vCustomerId = localStorage.getItem('customer_id');
console.log(vCustomerId + " Customerid");
var vAmount = 0;
var vDataCart = [];
var vDataOrder = [];
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onLoadInfoUser();
    onLoadOrder();
    onLoadCartByIdUser();

    $("#btn-payment").on("click", function () {
        console.log("id customer:" + vCustomerId);
        // onBtnAddPayment(vCustomerId);
        // onBtnAddOrder();
        onBtnAddOrderDetail();

        $("#follow-order").modal('show');
        $("#btn-follow").on("click", function () {
            window.location.href = "http://localhost:5500/FollowOrder.html"
        })
    })
    $("#btn-fake").on("click", function () {


        var vProductId = vDataCart[0].id;
        var vCus = vCustomerId;
        console.log(vProductId + " pro");
        console.log(vCus + " cus");
    })

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */


function onLoadInfoUser() {
    $.ajax({
        url: gBASE_URL + "users/user/" + vUserId,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {

            console.log(paramRes);
            handleLoadData(paramRes);
            // vCustomerId = paramRes.customer.id;

        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
function onLoadOrder() {
    $.ajax({
        url: gBASE_URL + "cart",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {

            console.log(paramRes);

            // handleFillOrder(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
function onLoadCartByIdUser() {
    $.ajax({
        url: gBASE_URL + "cart/" + vUserId,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            console.log("cart")
            console.log(paramRes);
            vDataCart = paramRes;
            console.log("success cart by Id user");
            handleFillOrder(paramRes);
            vAmount = handleTotalBill(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
function onBtnUpdateCustomer(vIdCustomer) {
    var vObj = {
        lastName: null,
        firstName: null,
        phoneNumber: null,
        address: null,
        city: null,
        state: null,
        postalCode: null,
        country: null,
        saleRepEmployeeNumber: 0,
        creditLimit: 0,
        payments: [],
        orders: []
    }

    thuThapThongTinInforCustomer(vObj);
    $.ajax({
        url: gBASE_URL + "customer/" + vIdCustomer,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),

        success: function (paramRes) {
            alert("success");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })

}
// function onBtnAddPayment(vIdCustomer) {
//     var vObj = {
//         ammount: "",
//         checkNumber: "",

//     }
//     vObj.ammount = vAmount;
//     console.log(vObj);
//     $.ajax({
//         url: gBASE_URL + "payment/" + vIdCustomer,
//         type: "POST",
//         headers: {
//             "Authorization": "Bearer " + accessToken,
//         },
//         dataType: "json",
//         contentType: "application/json; charset=utf-8",
//         data: JSON.stringify(vObj),
//         success: function (paramRes) {

//         },
//         error: function (paramErr) {
//             alert(paramErr.status);
//         }
//     })
// }

// function onBtnAddOrder() {
//     var vObj = {
//         comments: "",
//         // OrderDate: "",
//         // requiredDate: "",
//         // shippedDate: "",
//         status: "Đơn hàng đang được giao",

//     }

//     $.ajax({
//         url: gBASE_URL + "order/" + vCustomerId,
//         type: "POST",
//         headers: {
//             "Authorization": "Bearer " + accessToken,
//         },
//         dataType: "json",
//         contentType: "application/json; charset=utf-8",
//         data: JSON.stringify(vObj),
//         success: function (paramRes) {
//             console.log("add order success");
//         },
//         error: function (paramErr) {
//             alert(paramErr.status);
//         }
//     })

// }

// de lay Id order moi nhat
function onLoadOrder() {
    $.ajax({
        url: gBASE_URL + "order",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + accessToken,
        },
        success: function (paramRes) {
            vDataOrder = paramRes;
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }

    })
}
// detail Order
function onBtnAddOrderDetail() {
    // tim id order moi them vao
    var vLenOrder = vDataOrder.length;
    var vOrderId = vDataOrder[vLenOrder - 1].id;

    for (var bI = 0; bI < vDataCart.length; bI++) {
        var vObj = {
            priceEach: 0,
            quantityOrder: 0,
            productCartId: 0,
            customerCartId: vCustomerId,
            productName: "",
            photo: "",
            buyPrice: 0
        }

        vObj.priceEach = vDataCart[bI].totalBill;
        vObj.quantityOrder = vDataCart[bI].quantity;
        vObj.productCartId = vDataCart[bI].product.id;
        vObj.productName = vDataCart[bI].product.productName;
        vObj.photo = vDataCart[bI].product.photo;
        vObj.buyPrice = vDataCart[bI].product.buyPrice;
        // vObj.customerCartId = vCustomerId;
        console.log(vObj);


        $.ajax({
            url: gBASE_URL + "orderDetail/" + vDataCart[bI].product.id + "/" + vOrderId,
            type: "POST",
            headers: {
                "Authorization": "Bearer " + accessToken,
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vObj),
            success: function (paramRes) {
                console.log("them order detail thanh cong");
            },
            error: function (paramErr) {
                alert(paramErr.status);
            }

        })
    }

}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function handleLoadData(paramData) {
    // $("#inp-username").val(paramData.username);
    $("#inp-name").val(paramData.customer.firstName + " " + paramData.customer.lastName);

    $("#inp-email").val(paramData.email);
    $("#inp-phone").val(paramData.customer.phoneNumber);
    $("#inp-address").val(paramData.customer.address);
    $("#inp-country").val(paramData.customer.country);

}
function thuThapThongTinInforCustomer(paramObj) {
    paramObj.lastName = $("#inp-lname").val();
    paramObj.firstName = $("#inp-fname").val();
    paramObj.phoneNumber = $("#inp-phone").val();
    paramObj.address = $("#inp-address").val();
}
function handleFillOrder(paramData) {
    var vTotal = 0;
    for (var bI = 0; bI < paramData.length; bI++) {
        vTotal += paramData[bI].totalBill
        $("#order-title").append(`
            <div class="d-flex order-title">
                <p id="product-name" class="mr-auto p-2">${paramData[bI].product.productName} x ${paramData[bI].quantity}</p>
                <p id="buy-price" class="p-2">${paramData[bI].product.buyPrice}</p>

            </div>
        `)

    }

    $(".total-price").html("$ " + vTotal)
}
function handleTotalBill(paramData) {
    var vCount = 0;
    for (var bI = 0; bI < paramData.length; bI++) {
        vCount += paramData[bI].totalBill;
    }
    return vCount;
}



